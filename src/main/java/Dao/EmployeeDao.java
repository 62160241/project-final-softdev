/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author Sirapatson
 */
public class EmployeeDao implements DaoInterface<Employee> {

    // Add
    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int id = -1;
        try {
            String sql = "INSERT INTO Employee (Emp_Fname,Emp_Lname,"
                    + "Emp_Username,Emp_password,Emp_Tel,Emp_AccNumber,"
                    + "Emp_Position)VALUES (?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFname());
            stmt.setString(2, object.getLname());
            stmt.setString(3, object.getUsername());
            stmt.setString(4, object.getPassword());
            stmt.setString(5, object.getTel());
            stmt.setString(6, object.getAccNumber());
            stmt.setString(7, object.getPosition());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    // Get
    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "SELECT Emp_id,Emp_Fname,Emp_Lname,Emp_Username,"
                    + "Emp_password,Emp_Tel,Emp_AccNumber,Emp_Position FROM Employee;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Emp_id");
                String fname = result.getString("Emp_Fname");
                String lname = result.getString("Emp_Lname");
                String username = result.getString("Emp_Username");
                String password = result.getString("Emp_password");
                String tel = result.getString("Emp_Tel");
                String accNumber = result.getString("Emp_AccNumber");
                String position = result.getString("Emp_Position");
                Employee emp = new Employee(id, fname, lname, username, password, tel, accNumber, position);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "SELECT Emp_id,Emp_Fname,Emp_Lname,Emp_Username,"
                    + "Emp_password,Emp_Tel,Emp_AccNumber,Emp_Position FROM Employee WHERE Emp_id="
                    + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("Emp_id");
                String fname = result.getString("Emp_Fname");
                String lname = result.getString("Emp_Lname");
                String username = result.getString("Emp_Username");
                String password = result.getString("Emp_password");
                String tel = result.getString("Emp_Tel");
                String accNumber = result.getString("Emp_AccNumber");
                String position = result.getString("Emp_Position");
                Employee emp = new Employee(id, fname, lname, username, password, tel, accNumber, position);
                return emp;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    public Employee get(String Username, String Password) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "SELECT Emp_id,Emp_Fname,Emp_Lname,Emp_Username,"
                    + "Emp_password,Emp_Tel,Emp_AccNumber,Emp_Position FROM Employee WHERE Emp_Username =\"" + Username + "\" And Emp_password = \"" + Password + "\"";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int id = result.getInt("Emp_id");
                String fname = result.getString("Emp_Fname");
                String lname = result.getString("Emp_Lname");
                String username = result.getString("Emp_Username");
                String password = result.getString("Emp_password");
                String tel = result.getString("Emp_Tel");
                String accNumber = result.getString("Emp_AccNumber");
                String position = result.getString("Emp_Position");
                Employee emp = new Employee(id, fname, lname, username, password, tel, accNumber, position);
                return emp;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    // Delete
    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "DELETE FROM Employee WHERE Emp_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    // Update
    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "UPDATE Employee SET Emp_id = ?,Emp_Fname = ?,"
                    + "Emp_Lname = ?,Emp_Username = ?,Emp_password = ?,"
                    + "Emp_Tel = ?,Emp_AccNumber = ?,Emp_Position = ?"
                    + "WHERE Emp_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setString(2, object.getFname());
            stmt.setString(3, object.getLname());
            stmt.setString(4, object.getUsername());
            stmt.setString(5, object.getPassword());
            stmt.setString(6, object.getTel());
            stmt.setString(7, object.getAccNumber());
            stmt.setString(8, object.getPosition());
            stmt.setInt(9, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

}
