/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author acer
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int id = -1;
        try {
            String sql = "INSERT INTO Customer (Cus_Fname,Cus_Lname,"
                    + "Cus_Tel,Cus_Point,"
                    + "Cus_Purchases_Count)VALUES (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFname());
            stmt.setString(2, object.getLname());
            stmt.setString(3, object.getTel());
            stmt.setInt(4, object.getPoint());
            stmt.setInt(5, object.getCount());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "SELECT Cus_id,Cus_Fname,Cus_Lname,"
                    + "Cus_Tel,Cus_Point,Cus_Purchases_Count FROM Customer;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Cus_id");
                String fname = result.getString("Cus_Fname");
                String lname = result.getString("Cus_Lname");
                String tel = result.getString("Cus_Tel");
                int point = result.getInt("Cus_Point");
                int count = result.getInt("Cus_Purchases_Count");
                Customer cus = new Customer(id, fname, lname, tel, point, count);
                list.add(cus);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "SELECT Cus_id,Cus_Fname,Cus_Lname,"
                    + "Cus_Tel,Cus_Point,Cus_Purchases_Count FROM Customer WHERE Cus_id=" 
                    + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("Cus_id");
                String fname = result.getString("Cus_Fname");
                String lname = result.getString("Cus_Lname");
                String tel = result.getString("Cus_Tel");
                int point = result.getInt("Cus_Point");
                int count = result.getInt("Cus_Purchases_Count");
                Customer cus = new Customer(id, fname, lname, tel, point, count);
                return cus;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "DELETE FROM Customer WHERE Cus_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "UPDATE Customer SET Cus_id = ?,Cus_Fname = ?,"
                    + "Cus_Lname = ?,Cus_Tel = ?,Cus_Point = ?,Cus_Purchases_Count = ?"
                    + "WHERE Cus_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setString(2, object.getFname());
            stmt.setString(3, object.getLname());
            stmt.setString(4, object.getTel());
            stmt.setInt(5, object.getPoint());
            stmt.setInt(6, object.getCount());
            stmt.setInt(7, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    
    public Customer getMember(String tel) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "SELECT Cus_id,Cus_Fname,Cus_Lname,"
                    + "Cus_Tel,Cus_Point,Cus_Purchases_Count FROM Customer WHERE Cus_Tel=\"" 
                    + tel+"\"";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int id = result.getInt("Cus_id");
                String fname = result.getString("Cus_Fname");
                String lname = result.getString("Cus_Lname");
                String telp = result.getString("Cus_Tel");
                int point = result.getInt("Cus_Point");
                int count = result.getInt("Cus_Purchases_Count");
                Customer cus = new Customer(id, fname, lname, tel, point, count);
                return cus;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

}
