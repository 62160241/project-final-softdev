/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Promotion;

/**
 *
 * @author USER
 */
public class PromotionDao implements DaoInterface<Promotion> {

    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Override
    public int add(Promotion object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Promotion (Promo_Name,Promo_Start,Promo_End,"
                    + "Promo_Discount,Prose_id)VALUES (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, dateFormat.format(object.getStart()));
            stmt.setString(3, dateFormat.format(object.getEnd()));
            stmt.setDouble(4, object.getDiscount());
            stmt.setInt(5, object.getIds());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Promotion> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Promo_id,Promo_Name,Promo_Start,Promo_End,"
                    + "Promo_Discount,Prose_id FROM Promotion;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int Proid = result.getInt("Promo_id");
                String Name = result.getString("Promo_Name");
                Date Start = (Date) dateFormat.parse(result.getString("Promo_Start"));
                Date End = (Date) dateFormat.parse(result.getString("Promo_End"));
                Double Discount = result.getDouble("Promo_Discount");
                int Poseid = result.getInt("Prose_id");

                Promotion pmt = new Promotion(Proid, Name, Start, End, Discount, Poseid);
                list.add(pmt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Promotion get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Promo_id,Promo_Name,Promo_Start,Promo_End,"
                    + "Promo_Discount,Prose_id FROM Promotion WHERE Promo_id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int Proid = result.getInt("Promo_id");
                String Name = result.getString("Promo_Name");
                Date Start = (Date) dateFormat.parse(result.getString("Promo_Start"));
                Date End = (Date) dateFormat.parse(result.getString("Promo_End"));
                Double Discount = result.getDouble("Promo_Discount");
                int Poseid = result.getInt("Prose_id");

                Promotion pmt = new Promotion(Proid, Name, Start, End, Discount, Poseid);
                return pmt;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "DELETE FROM Promotion WHERE Promo_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Promotion object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Promotion SET Promo_id = ?,Promo_Name = ?,"
                    + "Promo_Start = ?,Promo_End = ?,Promo_Discount = ?,Prose_id=?"
                    + "WHERE Promo_id = ?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setString(2, object.getName());
            stmt.setString(3, dateFormat.format(object.getStart()));
            stmt.setString(4, dateFormat.format(object.getEnd()));
            stmt.setDouble(5, object.getDiscount());
            stmt.setInt(6, object.getIds());
            stmt.setInt(7, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

}
