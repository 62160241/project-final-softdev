/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Employee;
import model.ProductSell;
import model.Receipt;
import model.ReceiptDetail;

/**
 *
 * @author ckitt
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Reciept (Rec_time,Cus_id,Emp_id,Rec_Total) VALUES (?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, dateFormat.format(object.getTime()));
            stmt.setInt(2, object.getCus().getId());
            if (object.getEmp() == null) {
                stmt.setString(3, null);
            } else {
                stmt.setInt(3, object.getEmp().getId());
            }
            stmt.setDouble(4, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO Reciept_Detail (Recd_Price,Recd_Amount,Rec_id,Prose_id) VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setDouble(1, r.getPrice());
                stmtDetail.setInt(2, r.getAmount());
                stmtDetail.setInt(3, r.getReceipt().getId());
                stmtDetail.setInt(4, r.getProd().getId());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmt.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }

        } catch (SQLException ex) {
            System.out.println("Error : to create receipt");
        }
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT r.Rec_id as Receipt_id,\n"
                    + "       Rec_time,\n"
                    + "       c.Cus_id,\n"
                    + "       c.Cus_Fname as Customer_Fname,\n"
                    + "       c.Cus_Lname as Customer_Lname,\n"
                    + "       c.Cus_Tel as Customer_Tel,\n"
                    + "       c.Cus_Point as Customer_Point,\n"
                    + "       c.Cus_Purchases_Count as Customer_Purchases_Count,\n"
                    + "       u.Emp_id,\n"
                    + "       u.Emp_Fname as Employee_Fname,\n"
                    + "       u.Emp_Lname as Employee_Lname,\n"
                    + "       u.Emp_Username as Employee_Username,\n"
                    + "       u.Emp_Password as Employee_Password,\n"
                    + "       u.Emp_Tel as Employee_Tel,\n"
                    + "       u.Emp_AccNumber as Employee_AccNumber,\n"
                    + "       u.Emp_Position as Employee_Position,\n"
                    + "       Rec_Total\n"
                    + "  FROM Reciept r, Customer c, Employee u\n"
                    + "  WHERE r.Cus_id = c.Cus_id AND r.Emp_id = u.Emp_id;"
                    + "  ORDER BY created DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Receipt_id");
                Date created = (Date) dateFormat.parse(result.getString("Rec_time"));
                int customerId = result.getInt("Cus_id");
                String customerFName = result.getString("Customer_Fname");
                String customerLName = result.getString("Customer_Lname");
                String customerTel = result.getString("Customer_Tel");
                int point = Integer.parseInt(result.getString("Customer_Point"));
                int count = Integer.parseInt(result.getString("Customer_Purchases_Count"));
                int userId = result.getInt("Emp_id");
                String userFName = result.getString("Employee_Fname");
                String userLName = result.getString("Employee_Lname");
                String empUsername = result.getString("Employee_Username");
                String empPassword = result.getString("Employee_Password");
                String userTel = result.getString("Employee_Tel");
                String empAccNumber = result.getString("Employee_AccNumber");
                String empPosition = result.getString("Employee_Position");
                double total = result.getDouble("Rec_Total");
                Receipt receipt = new Receipt(id, created,
                        new Employee(userId, userFName, userLName, empUsername, empPassword, userTel, empAccNumber, empPosition),
                        new Customer(customerId, customerFName, customerLName, customerTel, point, count));
                getReceiptDetail(conn, id, receipt);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt!!!" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT r.Rec_id as Receipt_id,\n"
                    + "       Rec_time,\n"
                    + "       c.Cus_id,\n"
                    + "       c.Cus_Fname as Customer_Fname,\n"
                    + "       c.Cus_Lname as Customer_Lname,\n"
                    + "       c.Cus_Tel as Customer_Tel,\n"
                    + "       c.Cus_Point as Customer_Point,\n"
                    + "       c.Cus_Purchases_Count as Customer_Purchases_Count,\n"
                    + "       u.Emp_id,\n"
                    + "       u.Emp_Fname as Employee_Fname,\n"
                    + "       u.Emp_Lname as Employee_Lname,\n"
                    + "       u.Emp_Username as Employee_Username,\n"
                    + "       u.Emp_Password as Employee_Password,\n"
                    + "       u.Emp_Tel as Employee_Tel,\n"
                    + "       u.Emp_AccNumber as Employee_AccNumber,\n"
                    + "       u.Emp_Position as Employee_Position,\n"
                    + "       Rec_Total\n"
                    + "  FROM Reciept r, Customer c, Employee u\n"
                    + "  WHERE r.Rec_id = ? AND r.Cus_id = c.Cus_id AND r.Emp_id = u.Emp_id;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                int rid = result.getInt("Receipt_id");
                Date created = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(result.getString("Rec_time"));
                int customerId = result.getInt("Cus_id");
                String customerFName = result.getString("Customer_Fname");
                String customerLName = result.getString("Customer_Lname");
                String customerTel = result.getString("Customer_Tel");
                int point = Integer.parseInt(result.getString("Customer_Point"));
                int count = Integer.parseInt(result.getString("Customer_Purchases_Count"));
                int userId = result.getInt("Emp_id");
                String userFName = result.getString("Employee_Fname");
                String userLName = result.getString("Employee_Lname");
                String empUsername = result.getString("Employee_Username");
                String empPassword = result.getString("Employee_Password");
                String userTel = result.getString("Employee_Tel");
                String empAccNumber = result.getString("Employee_AccNumber");
                String empPosition = result.getString("Employee_Position");
                double total = result.getDouble("Rec_Total");
                Receipt receipt = new Receipt(rid, created,
                        new Employee(userId, userFName, userLName, empUsername, empPassword, userTel, empAccNumber, empPosition),
                        new Customer(customerId, customerFName, customerLName, customerTel, point, count));
                getReceiptDetail(conn, id, receipt);
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt!!!" + ex.getMessage());
        }
        db.close();
        return null;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) {
        try {
            String sqlDetail = "SELECT Recd_id as id,\n"
                    + "    p.Prose_name as Product_Name,\n"
                    + "    p.Prose_price as Product_Price,\n"
                    + "       rd.Recd_Price as price,\n"
                    + "       Recd_Amount,\n"
                    + "       Rec_id,\n"
                    + "       rd.Prose_id\n"
                    + "  FROM Reciept_Detail rd, ProductSell p\n"
                    + "  WHERE Rec_id = ? AND rd.Prose_id = p.Prose_id;";
            PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
            stmtDetail.setInt(1, id);
            ResultSet resultDetail = stmtDetail.executeQuery();

            while (resultDetail.next()) {
                int receiveId = resultDetail.getInt("id");
                int productId = resultDetail.getInt("Prose_id");
                String productName = resultDetail.getString("Product_Name");
                double productprice = resultDetail.getDouble("Product_Price");
                double price = resultDetail.getDouble("price");
                int amount = resultDetail.getInt("Recd_Amount");
                ProductSell product = new ProductSell(productId, productName, productprice);
                receipt.addReceiptDetail(receiveId, product, amount, price);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Reciept WHERE Rec_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect receipt id " + id + "!!!");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public double getMoneyDay(Date date) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        double total = 0;
        int x = date.getDate();
        int y = date.getDate() + 1;
        String da;
        String dc;

        if (x < 10) {
            da = "0" + x;
        } else {
            da = "" + x;
        }

        if (y < 10) {
            dc = "0" + y;
        } else {
            dc = "" + y;
        }

        try {
            String sql = "SELECT SUM(Rec_Total)\n"
                    + "  FROM Reciept\n"
                    + "  WHERE Rec_Time BETWEEN \"2564/11/" + da + "\" AND \"2564/11/" + dc + "\" ;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            total = result.getInt("SUM(Rec_Total)");
            return total;
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!!!" + ex.getMessage());
        }

        db.close();
        return 0;
    }

    public double getMoneyMounth(Date date) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        double total = 0;
        int x = date.getMonth();
        int y = date.getMonth();
        String da;
        String dc;
        if (x == 10 || x == 11) {
            da = "" + (x + 1);
        } else if(x==12) {
            da = "01";
        }else{
            da = "0" + (x + 1);
        }
        
        y=x+1;
        if (y == 10 || y == 11) {
            dc = "" + (y+1);
        } else if(y==12) {
            dc = "01";
        }else{
            dc = "0" + (y+1);
        }

        try {
            String sql = "SELECT SUM(Rec_Total)\n"
                    + "  FROM Reciept\n"
                    + "  WHERE Rec_Time BETWEEN \"2564/"+da+"/01\" AND \"2564/"+dc+"/01\";";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            total = result.getInt("SUM(Rec_Total)");
            return total;
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!!!" + ex.getMessage());
        }

        db.close();
        return 0;
    }
    
    public double getMoneyYear(Date date) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        double total = 0;
        int x = date.getYear()+543;
        int y = date.getMonth()+544;
        String da = ""+x;
        String dc = ""+y;

        try {
            String sql = "SELECT SUM(Rec_Total)\n"
                    + "  FROM Reciept\n"
                    + "  WHERE Rec_Time BETWEEN \"+"+da+"/01/01\" AND \""+dc+"/01/01\";";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            total = result.getInt("SUM(Rec_Total)");
            return total;
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!!!" + ex.getMessage());
        }

        db.close();
        return 0;
    }
}
