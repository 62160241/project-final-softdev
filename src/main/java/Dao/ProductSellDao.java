/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ProductSell;

/**
 *
 * @author AKYROS
 */
public class ProductSellDao {
    
    public int add(ProductSell object) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        int id = -1;
        try {
            String sql = "INSERT INTO ProductSell (Prose_id,Prose_name,Prose_price)\n" +
"                         VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setString(2, object.getName());
            stmt.setDouble(3, object.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductSellDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    
    public ArrayList<ProductSell> getAll() {
  ArrayList list = new ArrayList(); 
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT Prose_id,Prose_id,Prose_name,Prose_price,Prose_image" 
                    +"  FROM ProductSell;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Prose_id");
                String name = result.getString("Prose_name");
                Double price = result.getDouble("Prose_price");
                String image = result.getString("Prose_image");
                ProductSell prost = new ProductSell(id, name, price,image);
                list.add(prost);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductSellDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list; }

    public ProductSell get(int id) {
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "SELECT Prose_id,Prose_name,Prose_price,Prose_image" 
                    +"  FROM ProductSell WHERE Prose_id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()){
                int pid = result.getInt("Prose_id");
                String name = result.getString("Prose_name");
                Double price = result.getDouble("Prose_price");
                String image = result.getString("Prose_image");
                ProductSell prost = new ProductSell(id, name, price, image);
                return prost;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductSellDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "DELETE FROM ProductSell WHERE Prose_id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductSellDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    
    public int update(ProductSell object) {
           Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "UPDATE ProductSell SET Prose_id = ?,Prose_name = ?,"
                    +"Prose_price = ? WHERE Prose_id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setString(2, object.getName());
            stmt.setDouble(3, object.getPrice());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductStockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

}
