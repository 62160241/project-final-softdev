/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ProductStock;

/**
 *
 * @author Acer
 */
public class ProductStockDao implements DaoInterface<ProductStock> {

    @Override
    public int add(ProductStock object) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        int id = -1;
        try {
            String sql = "INSERT INTO ProductStock (Prost_name,Prost_amount,Prost_price)\n" +
"                         VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getAmount());
            stmt.setDouble(3, object.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductStockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<ProductStock> getAll() {
  ArrayList list = new ArrayList(); 
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT Prost_id,Prost_name,Prost_amount,Prost_price\n" 
                    +"  FROM ProductStock;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Prost_id");
                String name = result.getString("Prost_name");
                int amount = result.getInt("Prost_amount");
                Double price = result.getDouble("Prost_price");
                ProductStock prost = new ProductStock(id, name, amount, price);
                list.add(prost);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductStockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list; }

    @Override
    public ProductStock get(int id) {
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            String sql = "SELECT Prost_id,Prost_name,Prost_amount,Prost_price" 
                    +"  FROM ProductStock WHERE Prost_id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()){
                int pid = result.getInt("Prost_id");
                String name = result.getString("Prost_name");
                int amount = result.getInt("Prost_amount");
                Double price = result.getDouble("Prost_price");
                 ProductStock prost = new ProductStock(id, name, amount, price);
                return prost;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductStockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "DELETE FROM ProductStock WHERE Prost_id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductStockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(ProductStock object) {
           Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "UPDATE ProductStock SET Prost_id = ?,Prost_name = ?,"
                    + "Prost_amount = ?,Prost_price = ? WHERE Prost_id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setString(2, object.getName());
            stmt.setInt(3, object.getAmount());
            stmt.setDouble(4, object.getPrice());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductStockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    
  /*public static void main(String[] args) {
      ProductStockDao dao = new ProductStockDao();
      System.out.println(dao.getAll());
      System.out.println(dao.get(1));
     // int id = dao.add(new ProductStock(-1,"cocoa",10,300.0));
      //System.out.println("id: "+id);
      
     // ProductStock last = dao.get(3);
     // System.out.println("last: "+last);
     // last.setAmount(30);
      //int row =dao.update(last);
      //ProductStock updateProductStock = dao.get(3);
      //System.out.println("update "+updateProductStock);
      dao.delete(3);
      ProductStock deleteProductStock = dao.get(3);
      System.out.println("delete "+deleteProductStock);
  }  */
}
