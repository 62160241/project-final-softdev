/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Attendance;

/**
 *
 * @author suwat
 */
public class AttendanceDao implements DaoInterface<Attendance> {

    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Override
    public int add(Attendance object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Attendance (Att_Checkin, "
                    + "Att_Checkout,Att_TotalHour, Emp_id )VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, dateFormat.format(object.getAtt_checkin()));
            stmt.setString(2, dateFormat.format(object.getAtt_checkout()));
            stmt.setString(3, "" + object.getAtt_totalhour());
            stmt.setDouble(4, object.getEmpId());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Attendance> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        Date att_checkout;
        //process
        try {
            String sql = "SELECT Att_id,\n"
                    + "       Att_Checkin,\n"
                    + "       Att_Checkout,\n"
                    + "       Att_TotalHour,\n"
                    + "       Emp_id\n"
                    + "  FROM Attendance;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Att_id");
                Date att_checkin = (Date) dateFormat.parse(result.getString("Att_Checkin"));
                if (!result.getString("Att_Checkout").equals("")) {
                    att_checkout = (Date) dateFormat.parse(result.getString("Att_Checkout"));
                } else {
                    att_checkout = null;
                }
                double att_totalHour = result.getDouble("Att_TotalHour");
                int empid = result.getInt("Emp_id");
                Attendance cus = new Attendance(id, att_checkin, att_checkout, att_totalHour, empid);
                list.add(cus);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AttendanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Attendance get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        Date att_checkout;
        try {
            String sql = "SELECT Att_id,\n"
                    + "       Att_Checkin,\n"
                    + "       Att_Checkout,\n"
                    + "       Att_TotalHour,\n"
                    + "       Emp_id FROM Attendance WHERE Att_id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int att_id = result.getInt("Att_id");
                Date att_checkin = (Date) dateFormat.parse(result.getString("Att_Checkin"));
                if (!result.getString("Att_Checkout").equals("")) {
                    att_checkout = (Date) dateFormat.parse(result.getString("Att_Checkout"));
                } else {
                    att_checkout = null;
                }
                Double att_totalhour = result.getDouble("Att_TotalHour");
                int empid = result.getInt("Emp_id");

                Attendance att = new Attendance(att_id, att_checkin, att_checkout, att_totalhour, empid);
                return att;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        int row = 0;
        try {
            String sql = "DELETE FROM Attendance WHERE Att_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Attendance object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int checkIn(Attendance object) { //id, checkin, checkout, hour, empid
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Attendance (Att_Checkin, "
                    + "Att_Checkout,Att_TotalHour, Emp_id )VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, dateFormat.format(object.getAtt_checkin()));
            stmt.setString(2, "");
            stmt.setString(3, "" + object.getAtt_totalhour());
            stmt.setDouble(4, object.getEmpId());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    public int checkOut(Attendance object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Attendance SET Att_Checkin = ?,"
                    + "Att_Checkout = ?,Att_TotalHour = ?,Emp_id = ?"
                    + "WHERE Emp_id = ? AND Att_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, dateFormat.format(object.getAtt_checkin()));
            stmt.setString(2, dateFormat.format(object.getAtt_checkout()));
            stmt.setDouble(3, object.getAtt_totalhour());
            stmt.setInt(4, object.getEmpId());
            stmt.setInt(5, object.getEmpId());
            stmt.setInt(6, object.getAtt_id());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    public Attendance checkDate(Date date, int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        Date att_checkout;
        String day;
        if (date.getDate() < 10) {
            day = "0" + date.getDate();
        } else {
            day = "" + date.getDate();
        }

        try {
            String sql = "SELECT Att_id, Att_Checkin,"
                    + " Att_Checkout,Att_TotalHour,"
                    + " Emp_id FROM Attendance"
                    + " WHERE (Att_Checkin Between \"2564/11/" + day + " 00:00:00\" AND \"2564/11/" + day + " 23:59:59\") AND Emp_id = +" + id + ";";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int att_id = result.getInt("Att_id");
                Date att_checkin = (Date) dateFormat.parse(result.getString("Att_Checkin"));
                if (!result.getString("Att_Checkout").equals("")) {
                    att_checkout = (Date) dateFormat.parse(result.getString("Att_Checkout"));
                } else {
                    att_checkout = null;
                }
                Double att_totalhour = result.getDouble("Att_TotalHour");
                int empid = result.getInt("Emp_id");

                Attendance att = new Attendance(att_id, att_checkin, att_checkout, att_totalhour, empid);
                return att;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    public ArrayList<Attendance> getAttendance(int id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process
        try {
            Date att_checkout;
            String sql = "SELECT Att_id,\n"
                    + "       Att_Checkin,\n"
                    + "       Att_Checkout,\n"
                    + "       Att_TotalHour,\n"
                    + "       Emp_id\n"
                    + "  FROM Attendance WHERE Emp_id =" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int pid = result.getInt("Att_id");
                Date att_checkin = (Date) dateFormat.parse(result.getString("Att_Checkin"));
                if (!result.getString("Att_Checkout").equals("")) {
                    att_checkout = (Date) dateFormat.parse(result.getString("Att_Checkout"));
                } else {
                    att_checkout = null;
                }
                double att_totalHour = result.getDouble("Att_TotalHour");
                int empid = result.getInt("Emp_id");
                Attendance cus = new Attendance(pid, att_checkin, att_checkout, att_totalHour, empid);
                list.add(cus);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AttendanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }
    
    public ArrayList<Attendance> getHours(int id, int mount) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        Date att_checkout;
        String currentmount;
        if (mount < 10) {
            currentmount = "0" + mount;
        } else {
            currentmount = "" + mount;
        }
        //process
        try {
            String sql = "SELECT Att_id,Att_Checkin,Att_Checkout,Att_TotalHour,"
                    + "Emp_id FROM Attendance WHERE (Att_Checkin Between "
                    + "\"2564/" + mount + "/01\" AND \"2564/" + mount + "/31\") AND Emp_id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int pid = result.getInt("Att_id");
                Date att_checkin = (Date) dateFormat.parse(result.getString("Att_Checkin"));
                if (!result.getString("Att_Checkout").equals("")) {
                    att_checkout = (Date) dateFormat.parse(result.getString("Att_Checkout"));
                } else {
                    att_checkout = null;
                }
                double att_totalHour = result.getDouble("Att_TotalHour");
                int empid = result.getInt("Emp_id");
                Attendance cus = new Attendance(pid, att_checkin, att_checkout, att_totalHour, empid);
                list.add(cus);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AttendanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }
}
