/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nitroez.inthanincoffee;

import Dao.AttendanceDao;
import Service.EmployeeService;
import java.util.ArrayList;
import java.util.Date;
import model.Attendance;
import model.Employee;

/**
 *
 * @author Sirapatson
 */
public class SalaryPanel extends javax.swing.JPanel {

    /**
     * Creates new form SalaryPanel
     */
    private mainFrame mainFrame;
    private MainMenuPanel mainMenu;
    Employee emp = EmployeeService.salaryEmp;
    Attendance att;
    double salarylate;
    double salary;
    AttendanceDao dao = new AttendanceDao();
    
    public SalaryPanel(mainFrame mainFrame,MainMenuPanel mainMenu) {
        initComponents();
        this.mainFrame = mainFrame;
        this.mainMenu = mainMenu;
        loadEmpToForm();
    }
    
    public double getSumHours(ArrayList<Attendance> list){
        double sumHours = 0;
        for(Attendance att:list){
            sumHours += att.getAtt_totalhour();
        }
        return sumHours;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblId = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblType = new javax.swing.JLabel();
        lblNumber = new javax.swing.JLabel();
        lblHour = new javax.swing.JLabel();
        lblSalaruPerhr = new javax.swing.JLabel();
        lblSalary = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        txtIdFromEmp = new javax.swing.JTextField();
        txtFnameFromEmp = new javax.swing.JTextField();
        txtTypeFromEmp = new javax.swing.JTextField();
        txtAccNumFromEmp = new javax.swing.JTextField();
        txtWorkingHourFromAtt = new javax.swing.JTextField();
        txtSalaryPerhr = new javax.swing.JTextField();
        txtSalary = new javax.swing.JTextField();
        txtLnameFromEmp = new javax.swing.JTextField();

        lblId.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblId.setText("Employee Id :");

        lblName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblName.setText("Employee Name :");

        lblType.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblType.setText("Employee Type :");

        lblNumber.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblNumber.setText("Account Number :");

        lblHour.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblHour.setText("Workinng Hour :");

        lblSalaruPerhr.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblSalaruPerhr.setText("Salary Criteria (per hr) :");

        lblSalary.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblSalary.setText("Total Salary :");

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblId, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblType, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblHour, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSalaruPerhr, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtIdFromEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTypeFromEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAccNumFromEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtWorkingHourFromAtt, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSalaryPerhr, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(txtFnameFromEmp)
                                .addGap(18, 18, 18)
                                .addComponent(txtLnameFromEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 78, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblId, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIdFromEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFnameFromEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLnameFromEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblType, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTypeFromEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAccNumFromEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblHour, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtWorkingHourFromAtt, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSalaruPerhr, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSalaryPerhr, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(75, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        mainMenu.switchToEmpPanel();
    }//GEN-LAST:event_btnBackActionPerformed

    private void loadEmpToForm(){
        Date date = new Date();
        txtIdFromEmp.setText("" + emp.getId());
        txtFnameFromEmp.setText("" + emp.getFname());
        txtLnameFromEmp.setText("" + emp.getLname());
        txtTypeFromEmp.setText("" + emp.getPosition());
        txtAccNumFromEmp.setText("" + emp.getAccNumber());
        txtWorkingHourFromAtt.setText("" + getSumHours(dao.getHours(emp.getId(), date.getMonth() + 1)));
        if(emp.getPosition().equals("Employee")){
            txtSalaryPerhr.setText("45"); 
            salarylate = 45;
            salary = salarylate*getSumHours(dao.getHours(emp.getId(), date.getMonth() + 1));
        }else{
            txtSalaryPerhr.setText("55"); 
            salarylate = 55;
            salary = salarylate*getSumHours(dao.getHours(emp.getId(), date.getMonth()+ 1));
        }
        txtSalary.setText("" + salary);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JLabel lblHour;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNumber;
    private javax.swing.JLabel lblSalaruPerhr;
    private javax.swing.JLabel lblSalary;
    private javax.swing.JLabel lblType;
    private javax.swing.JTextField txtAccNumFromEmp;
    private javax.swing.JTextField txtFnameFromEmp;
    private javax.swing.JTextField txtIdFromEmp;
    private javax.swing.JTextField txtLnameFromEmp;
    private javax.swing.JTextField txtSalary;
    private javax.swing.JTextField txtSalaryPerhr;
    private javax.swing.JTextField txtTypeFromEmp;
    private javax.swing.JTextField txtWorkingHourFromAtt;
    // End of variables declaration//GEN-END:variables
}
