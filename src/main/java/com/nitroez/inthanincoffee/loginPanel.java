/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nitroez.inthanincoffee;

import Dao.EmployeeDao;
import Service.EmployeeService;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.Employee;

/**
 *
 * @author ckitt
 */
public class loginPanel extends javax.swing.JPanel {

    /**
     * Creates new form TestLogin
     */
    private mainFrame mainFrame;

    public loginPanel(mainFrame mainFrame) {
        initComponents();
        this.mainFrame = mainFrame;
        setPic();
    }
    
    void setPic(){
        ImageIcon imageIcon = new ImageIcon(new ImageIcon(".//Icon//login.jpg").getImage().getScaledInstance(1200, 800, Image.SCALE_DEFAULT));
        txtPic.setIcon(imageIcon);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtUsername = new javax.swing.JTextField();
        txtPassword = new javax.swing.JPasswordField();
        btnLogin = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtPic = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(1150, 780));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtUsername.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 440, 203, -1));

        txtPassword.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPasswordActionPerformed(evt);
            }
        });
        add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 480, 203, -1));

        btnLogin.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        add(btnLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 520, 230, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Password :");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 480, 90, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Username :");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 440, -1, -1));

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 420, 340, 150));
        add(txtPic, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1200, 800));
    }// </editor-fold>//GEN-END:initComponents

    private void txtPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPasswordActionPerformed

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        EmployeeDao dao = new EmployeeDao();
        String username = txtUsername.getText();
        String password = new String(txtPassword.getPassword());
        EmployeeService.currentEmp = dao.get(username, password);
        checkPos();
    }//GEN-LAST:event_btnLoginActionPerformed

    void checkPos() {
        if (EmployeeService.currentEmp != null) {
            if (EmployeeService.currentEmp.getPosition().equals("Manager")) {
                // Manager
                System.out.println("Manager");
                mainFrame.switchToMainMenu();
            } else {
                // Employee
                System.out.println("Employee");
                mainFrame.switchToMainMenuEmployee();
            }
        } else {
            OptionPaneExample();
            clearTextField();
        }
    }

    void OptionPaneExample() {
        JFrame f = new JFrame();
        JOptionPane.showMessageDialog(f, "Wrong Username, Password !!! ", "Alert", JOptionPane.WARNING_MESSAGE);
    }

    void clearTextField() {
        txtUsername.setText("");
        txtPassword.setText("");
        txtUsername.requestFocus();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JLabel txtPic;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
