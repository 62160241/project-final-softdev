/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nitroez.inthanincoffee;

import Dao.ProductSellDao;
import Service.ProductSellService;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import model.ProductSell;

/**
 *
 * @author USER
 */
public class ProductSellPanel extends javax.swing.JPanel {

    /**
     * Creates new form ProductSellPanel
     */
    private ArrayList<ProductSell> ProseList;
    private ProductSellTableModel model;
    private MainMenuPanel mainMenu;
    ProductSell editedProse;
    private ProductSellDao dao = new ProductSellDao();

    public ProductSellPanel(MainMenuPanel mainMenu) {
        initComponents();
        this.mainMenu = mainMenu;
        ProductSellDao dao = new ProductSellDao();
        loadTable(dao);
    }
    
    public void loadTable(ProductSellDao dao) {
        ProseList = dao.getAll();
        model = new ProductSellTableModel(ProseList);
        tblSell.setModel((TableModel) model);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblSell = new javax.swing.JTable();
        txtSearchProsell = new javax.swing.JTextField();
        btnSearchProsell = new javax.swing.JButton();
        btnAddProsell = new javax.swing.JButton();
        btnEditProsell = new javax.swing.JButton();
        btnDeleteProsell = new javax.swing.JButton();

        tblSell.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblSell);

        txtSearchProsell.setText("Search");
        txtSearchProsell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchProsellActionPerformed(evt);
            }
        });

        btnSearchProsell.setText("Search");
        btnSearchProsell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProsellActionPerformed(evt);
            }
        });

        btnAddProsell.setText("ADD");
        btnAddProsell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddProsellActionPerformed(evt);
            }
        });

        btnEditProsell.setText("EDIT");
        btnEditProsell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditProsellActionPerformed(evt);
            }
        });

        btnDeleteProsell.setText("DALETE");
        btnDeleteProsell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteProsellActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 803, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtSearchProsell, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearchProsell)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAddProsell)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnEditProsell)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDeleteProsell)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSearchProsell, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearchProsell))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 584, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddProsell, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEditProsell, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDeleteProsell, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtSearchProsellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchProsellActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchProsellActionPerformed

    private void btnAddProsellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddProsellActionPerformed
        mainMenu.switchToEditProductSellPanel();
    }//GEN-LAST:event_btnAddProsellActionPerformed

    private void btnEditProsellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditProsellActionPerformed
        if (tblSell.getSelectedRow() >= 0) {
           int check = Integer.parseInt(model.getValueAt(tblSell.getSelectedRow(), 0).toString());
            ProductSellService.editProse(check);
            mainMenu.switchToEditProductSellPanel();
        }
    }//GEN-LAST:event_btnEditProsellActionPerformed

    private void btnDeleteProsellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteProsellActionPerformed
        if (tblSell.getSelectedRow() >= 0) {
            int check = Integer.parseInt(model.getValueAt(tblSell.getSelectedRow(), 0).toString());
            editedProse = dao.get(check);
            int reply = JOptionPane.showConfirmDialog(null, "You want Delete?"
                    + editedProse.getId() + " !!!", "Warning", JOptionPane.YES_NO_OPTION );
            if (reply == JOptionPane.YES_OPTION) {
                dao.delete(editedProse.getId());
               txtSearchProsell.setText("");
            }
            clearSearch();
        }
         refershTable();
    }//GEN-LAST:event_btnDeleteProsellActionPerformed

    private void btnSearchProsellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProsellActionPerformed
        int id = Integer.parseInt(txtSearchProsell.getText());
        ProseList.clear();
        ProseList.add(dao.get(id));
        model = new ProductSellTableModel(ProseList);
        tblSell.setModel(model); 
    }//GEN-LAST:event_btnSearchProsellActionPerformed

    public void refershTable(){
        ArrayList<ProductSell> newList = dao.getAll();
        ProseList.clear();
        ProseList.addAll(newList);
        tblSell.revalidate();
        tblSell.repaint();
    }
    void clearSearch() {
        txtSearchProsell.setText("");
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddProsell;
    private javax.swing.JButton btnDeleteProsell;
    private javax.swing.JButton btnEditProsell;
    private javax.swing.JButton btnSearchProsell;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblSell;
    private javax.swing.JTextField txtSearchProsell;
    // End of variables declaration//GEN-END:variables

    private static class ProductSellTableModel extends AbstractTableModel {

        private ArrayList<ProductSell> data;
        String[] columName = {"ID", "Name", "Price"};

        public ProductSellTableModel(ArrayList<ProductSell> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            ProductSell prost = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return prost.getId();
            }
            if (columnIndex == 1) {
                return prost.getName();
            }

            if (columnIndex == 2) {
                return prost.getPrice();
            }
            
            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columName[column];
        }
    }
}
