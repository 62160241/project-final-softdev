/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class;

import model.ProductSell;

/**
 *
 * @author ckitt
 */
public class ProductBuy {
    ProductSell prose;
    int amount;

    public ProductBuy(ProductSell prose, int amount) {
        this.prose = prose;
        this.amount = amount;
    }

    public ProductSell getProse() {
        return prose;
    }

    public void setProse(ProductSell prose) {
        this.prose = prose;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    public void plusAmount(int amount) {
        this.amount += amount;
    }

    @Override
    public String toString() {
        return "ProductBuy{" + "prose=" + prose + ", amount=" + amount + '}';
    }
    
    
}
