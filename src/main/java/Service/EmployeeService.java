/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Dao.EmployeeDao;
import model.Employee;

/**
 *
 * @author ckitt
 */
public class EmployeeService {
    private static EmployeeDao dao = new EmployeeDao();
    public static Employee currentEmp;
    public static Employee salaryEmp;
    public static Employee editEmp;
    
    public static void clearEmp(){
        currentEmp=null;
    }
    
    public static void salaryEmp(int emp){
        salaryEmp = dao.get(emp);
    }
    
    public static void editEmp(int edit){
        editEmp = dao.get(edit);
    }
    
}
