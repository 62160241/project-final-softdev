/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Dao.CustomerDao;
import model.Customer;

/**
 *
 * @author acer
 */
public class CustomerSevice {
    private static CustomerDao dao = new CustomerDao();
    public static Customer currentCus;
    public static Customer salaryCus;
    public static Customer editCus;
    
    public static void clearCus(){
        currentCus=null;
    }
    
    public static void salaryCus(int cus){
        salaryCus = dao.get(cus);
    }
    
    public static void editCus(int edit){
        editCus = dao.get(edit);
    }
}
