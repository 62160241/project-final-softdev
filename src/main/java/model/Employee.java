/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sirapatson
 */
public class Employee {
    private int id;
    private String fname;
    private String lname;
    private String username;
    private String password;
    private String tel;
    private String accNumber;
    private String Position;

    public Employee(String fname, String lname, String username, String password, String tel, String accNumber, String Position) {
        this.fname = fname;
        this.lname = lname;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.accNumber = accNumber;
        this.Position = Position;
    }

    public Employee(int id, String fname, String lname, String username, String password, String tel, String accNumber, String Position) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.accNumber = accNumber;
        this.Position = Position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String Position) {
        this.Position = Position;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", fname=" + fname + ", lname=" + lname + ", username=" + username + ", password=" + password + ", tel=" + tel + ", accNumber=" + accNumber + ", Position=" + Position + '}';
    }
    
     
}
