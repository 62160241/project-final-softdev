/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author suwat
 */
public class Attendance {
    private int att_id;
    private Date att_checkin;
    private Date att_checkout;
    private double att_totalhour;
    private int empId;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public Attendance(int att_id, Date att_checkin, Date att_checkout, double att_totalhour, int empId) {
        this.att_id = att_id;
        this.att_checkin = att_checkin;
        this.att_checkout = att_checkout;
        this.att_totalhour = att_totalhour;
        this.empId = empId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getAtt_id() {
        return att_id;
    }

    public void setAtt_id(int att_id) {
        this.att_id = att_id;
    }

    public Date getAtt_checkin() {
        return att_checkin;
    }

    public void setAtt_checkin(Date att_checkin) {
        this.att_checkin = att_checkin;
    }

    public Date getAtt_checkout() {
        return att_checkout;
    }

    public void setAtt_checkout(Date att_checkout) {
        this.att_checkout = att_checkout;
    }

    public double getAtt_totalhour() {
        return att_totalhour;
    }

    public void setAtt_totalhour(double att_totalhour) {
        this.att_totalhour = att_totalhour;
    }
    
    public String format(Date date){
        if(date==null){
            return null;
        }
        return dateFormat.format(date);
    }
    @Override
    public String toString() {
        return "Attendance{" + "att_id=" + att_id + ", att_checkin=" + format(att_checkin) + ", att_checkout=" + format(att_checkout) + ", att_totalhour=" + att_totalhour + ", empId=" + empId + '}';
    }

}

