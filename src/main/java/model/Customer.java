/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author acer
 */
public class Customer {

    private int id;
    private String fname;
    private String lname;
    private String tel;
    private int point;
    private int count;

    public Customer(int id, String fname, String lname, String tel, int point, int count) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.tel = tel;
        this.point = point;
        this.count = count;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
    
    public void plusPoint(int point) {
        this.point += point;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    public void upCount() {
        this.count++;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", fname=" + fname + ", lname=" + lname + ", tel=" + tel + ", point=" + point + ", count=" + count + '}';
    }
    
}
