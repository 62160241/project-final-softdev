/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author AKYROS
 */
public class Promotion {
    private int id;
    private String name;
    private Date start;
    private Date end;
    private double discount;
    private int ids;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public Promotion(int id, String name, Date start, Date end, double discount, int ids) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
        this.discount = discount;
        this.ids = ids;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getIds() {
        return ids;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }
    public String format(Date date){
        return dateFormat.format(date);
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", start=" + format(start) + ", end=" + format(end) + ", discount=" + discount + ", ids=" + ids + '}';
    }
    
    
}
